<?php

// [ 路由 ]

return [

    // [ global ]
    // +----------------------------------------------
    '__miss__' => 'common/miss/index',
    // +----------------------------------------------

    // [ api ]
    // +----------------------------------------------
    'api$'     => 'api/index/index',
    // +----------------------------------------------
    
    // [ index ]
    // +----------------------------------------------
    '/'        => 'index/index/index',
    // +----------------------------------------------
    
    // [ admin ]
    // +----------------------------------------------
    // 后台
    'admin$'        => 'admin/index/index',
    // 后台登录
    'admin/login$'  => 'admin/login/index',
    // 后台管理
    'admin/system$' => 'admin/system/index',
    // 修改密码
    'admin/repwd$'  => 'admin/system/repwd',
    // 键值对
    'admin/keyvalue$'  => 'admin/keyvalue/index',
    'admin/keyvalue/add'  => 'admin/keyvalue/add',
    'admin/keyvalue/edit'  => 'admin/keyvalue/edit',
    // 管理员
    'admin/manager$'  => 'admin/manager/index',
    'admin/manager/add'  => 'admin/manager/add',
    'admin/manager/edit'  => 'admin/manager/edit',
    // 角色
    'admin/role$'  => 'admin/role/index',
    'admin/role/add'  => 'admin/role/add',
    'admin/role/edit'  => 'admin/role/edit',
    'admin/role/aut'  => 'admin/role/aut',

    // 节点
    'admin/node$'  => 'admin/node/index',
    'admin/node/add'  => 'admin/node/add',
    'admin/node/addchild'  => 'admin/node/addchild',
    'admin/node/edit'  => 'admin/node/edit',
    // +----------------------------------------------
    
];
