<?php

// 新增键值对
// POST kv.add

namespace app\api\resource\kv;
use think\Db;

class Add
{
    // 方法
    public $_method = ['POST'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '新增键值对';
    // 参数
    public $_param  = [
        'key'   => '键名',
        'value' => '键值',
    ];

    public function run(&$request)
    {
        $key   = $request->param('key/s','','trim');
        $value = $request->param('value/s','','trim');

        if ( empty($key) ) return [422,'键名必须'];

        $ret = Db::name('kv')->where('key',$key)->find();
        if ( !is_null($ret) ) return [400,'键名已存在'];

        $data['key'] = $key;
        $data['value'] = $value;
        $num = Db::name('kv')->insert($data);

        return ($num > 0) ? [201,'新增成功'] : 500;
    }
}