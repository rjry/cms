<?php

// 获取键值对详情
// GET kv.detail

namespace app\api\resource\kv;
use think\Db;

class Detail
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = [];
    // 描述
    public $_description = '获取键值对详情';
    // 参数
    public $_param  = [
        'key' => '键名',
    ];

    public function run(&$request)
    {
        $key = $request->param('key/s','','trim');

        if ( empty($key) ) return [422,'键名必须'];

        $ret = Db::name('kv')->where('key',$key)->find();
        if ( is_null($ret) ) return [404,'键值记录不存在'];

        return [200,$ret];
    }
}