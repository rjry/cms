<?php

// 删除键值对
// DELETE kv.delete

namespace app\api\resource\kv;
use think\Db;

class Delete
{
    // 方法
    public $_method = ['DELETE','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '删除键值对';
    // 参数
    public $_param  = [
        'key' => '键名',
    ];

    public function run(&$request)
    {
        $key = $request->param('key/s','','trim');

        if ( empty($key) ) return [422,'键名必须'];

        $num = Db::name('kv')->where('key',$key)->delete();

        return ($num > 0) ? [204,'删除成功'] : 500;
    }
}