<?php

// 编辑键值对
// PUT kv.edit

namespace app\api\resource\kv;
use think\Db;

class Edit
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '编辑键值对';
    // 参数
    public $_param  = [
        'key'   => '键名',
        'value' => '键值',
    ];

    public function run(&$request)
    {
        $key   = $request->param('key/s','','trim');
        $value = $request->param('value/s','','trim');

        if ( empty($key) ) return [422,'键名必须'];

        $ret = Db::name('kv')->where('key',$key)->find();
        if ( is_null($ret) ) return [404,'键值记录不存在'];

        $num = Db::name('kv')->where('key',$key)->update(['value'=>$value]);

        return ($num >= 0) ? [201,'编辑成功'] : 500;
    }
}