<?php

// 获取键值对列表
// GET kv.volist

namespace app\api\resource\kv;
use think\Db;

class Volist
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '获取键值对列表';
    // 参数
    public $_param  = [];

    public function run(&$request)
    {
        $list = Db::name('kv')->order('key')->select();

        return [200,$list];
    }
}