<?php

// 获取角色详情
// GET admin.rbac.role.detail

namespace app\api\resource\admin\rbac\role;
use think\Db;
use app\common\validate\Role as RoleValidate;

class Detail
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '获取角色详情';
    // 参数
    public $_param  = [
        'id' => '主键',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new RoleValidate;
        if ( !$validate->scene('detail')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $ret = Db::name('rbac_role')->where('id',$params['id'])->find();

        return [200,$ret];
    }
}