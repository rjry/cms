<?php

// 获取角色列表
// GET admin.rbac.role.volist

namespace app\api\resource\admin\rbac\role;
use think\Db;

class Volist
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '获取角色列表';
    // 参数
    public $_param  = [];

    public function run(&$request)
    {
        $list = Db::name('rbac_role')->order('id DESC')->select();

        return [200,$list];
    }
}
