<?php

// 删除角色
// DELETE admin.rbac.role.delete

namespace app\api\resource\admin\rbac\role;
use think\Db;
use app\common\validate\Role as RoleValidate;

class Delete
{
    // 方法
    public $_method = ['DELETE','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '删除角色';
    // 参数
    public $_param  = [
        'id' => '主键',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new RoleValidate;
        if ( !$validate->scene('delete')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $ret = Db::name('rbac_manager')->where('role_id',$params['id'])->find();
        if ( !is_null($ret) ) return [400,'角色下尚有管理员,不能执行此操作'];

        $num = Db::name('rbac_role')->where('id',$params['id'])->delete();

        return ($num > 0) ? [204,'删除成功'] : 500;
    }
}