<?php

// 新增角色
// POST admin.rbac.role.add

namespace app\api\resource\admin\rbac\role;
use think\Db;
use app\common\validate\Role as RoleValidate;

class Add
{
    // 方法
    public $_method = ['POST'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '新增角色';
    // 参数
    public $_param  = [
        'name'        => '角色名',
        'description' => '描述',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new RoleValidate;
        if ( !$validate->scene('add')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $ret = Db::name('rbac_role')->where('name',$params['name'])->find();
        if ( !is_null($ret) ) return [400,'角色名已存在'];

        $data['name']        = $params['name'];
        $data['description'] = $params['description'];
        $num = Db::name('rbac_role')->insert($data);

        return ($num > 0) ? [201,'新增成功'] : 500;
    }
}