<?php

// 分配角色权限节点
// PUT admin.rbac.role.assign

namespace app\api\resource\admin\rbac\role;
use think\Db;
use app\common\validate\Role as RoleValidate;

class Assign
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '分配角色权限节点';
    // 参数
    public $_param  = [
        'role_id' => '角色主键',
        'item'    => '节点,JsonArray',
    ];

    public function run(&$request)
    {
        $rid  = $request->param('role_id/d',0);
        $item = $request->param('item/a',[]);

        if ( empty($rid) ) return [422,'角色主键必须'];

        // 启动事务
        Db::startTrans();
        try{

            Db::name('rbac_role_node')->where('role_id',$rid)->delete();
            if ( !empty($item) ) {
                $data = [];
                foreach ($item as $key => $val) {
                    $data[] = [
                        'role_id' => $rid,
                        'node_id' => $val,
                    ];                
                }
                Db::name('rbac_role_node')->insertAll($data);
            }

            // 提交事务
            Db::commit();

            return [201,'分配成功'];

        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();

            return 500;
        }
    }
}