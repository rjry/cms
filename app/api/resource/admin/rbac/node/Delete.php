<?php

// 删除节点
// DELETE admin.rbac.node.delete

namespace app\api\resource\admin\rbac\node;
use think\Db;
use app\common\validate\Node as NodeValidate;

class Delete
{
    // 方法
    public $_method = ['DELETE','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '删除节点';
    // 参数
    public $_param  = [
        'id' => '主键',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new NodeValidate;
        if ( !$validate->scene('delete')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $num = Db::name('rbac_node')->where('id|pid',$params['id'])->delete();

        return ($num > 0) ? [204,'删除成功'] : 500;
    }
}