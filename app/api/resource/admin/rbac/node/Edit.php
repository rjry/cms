<?php

// 编辑节点
// PUT admin.rbac.node.edit

namespace app\api\resource\admin\rbac\node;
use think\Db;
use app\common\validate\Node as NodeValidate;

class Edit
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '编辑节点';
    // 参数
    public $_param  = [
        'id'          => '主键',
        'title'       => '名称',
        'description' => '描述',
        'url'         => 'URL',
        'pid'         => 'PID',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new NodeValidate;
        if ( !$validate->scene('edit')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $data['title']       = $params['title'];
        $data['description'] = $params['description'];
        $data['url']         = $params['url'];
        $data['pid']         = $params['pid'];
        $num = Db::name('rbac_node')->where('id',$params['id'])->update($data);

        return ($num >= 0) ? [201,'编辑成功'] : 500;
    }
}