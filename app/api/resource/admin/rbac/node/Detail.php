<?php

// 获取节点详情
// GET admin.rbac.node.detail

namespace app\api\resource\admin\rbac\node;
use think\Db;
use app\common\validate\Node as NodeValidate;

class Detail
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '获取节点详情';
    // 参数
    public $_param  = [
        'id' => '主键',
    ];

    public function run(&$request)
    {
        $params = $request->param();

        $validate = new NodeValidate;
        if ( !$validate->scene('detail')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $ret = Db::name('rbac_node')->where('id',$params['id'])->find();

        return [200,$ret];
    }
}
