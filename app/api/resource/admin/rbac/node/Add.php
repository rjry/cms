<?php

// 新增节点
// POST admin.rbac.node.add

namespace app\api\resource\admin\rbac\node;
use think\Db;
use app\common\validate\Node as NodeValidate;

class Add
{
    // 方法
    public $_method = ['POST'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '新增节点';
    // 参数
    public $_param  = [
        'title'       => '名称',
        'description' => '描述',
        'url'         => 'URL',
        'pid'         => 'PID',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new NodeValidate;
        if ( !$validate->scene('add')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $data['title']       = $params['title'];
        $data['description'] = $params['description'];
        $data['url']         = $params['url'];
        $data['pid']         = $params['pid'];
        $num = Db::name('rbac_node')->insert($data);

        return ($num > 0) ? [201,'新增成功'] : 500;
    }
}