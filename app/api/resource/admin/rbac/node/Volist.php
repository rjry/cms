<?php

// 获取(角色)权限节点列表
// GET admin.rbac.node.volist

namespace app\api\resource\admin\rbac\node;
use think\Db;

class Volist
{
    // 方法
    public $_method = ['GET'];
    // 前置
    // public $_pre    = ['manager'];
    public $_pre    = [];
    // 描述
    public $_description = '获取(角色)权限节点列表';
    // 参数
    public $_param  = [
        'role_id' => '角色主键,可选',
    ];

    public function run(&$request)
    {
        $rid = $request->param('role_id',0);

        if (0 == $rid) {
            // 超级管理员角色权限节点
            $list = Db::name('rbac_node')->select();
        } else {
            // 一般角色权限节点
            $item = Db::name('rbac_role_node')->where('role_id',$rid)->column('node_id');
            $list = empty($item) ? [] : Db::name('rbac_node')->where('id','in',$item)->select();
        }

        if ( !empty($list) ) $list = merge_node($list);

        // p($list);die;

        return [200,$list];
    }
}
