<?php

// 登出
// DELETE admin.rbac.manager.logout

namespace app\api\resource\admin\rbac\manager;
use think\facade\Session;

class Logout
{
    // 方法
    public $_method = ['DELETE','OPTIONS'];
    // 前置
    public $_pre    = [];
    // 描述
    public $_description = '登出';
    // 参数
    public $_param  = [];

    public function run(&$request)
    {
        Session::delete('manager');

        return [204,'登出成功'];
    }
}