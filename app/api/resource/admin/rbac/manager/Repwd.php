<?php

// 重置密码
// PUT admin.rbac.manager.repwd

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use think\facade\Session;
use app\common\validate\Manager as ManagerValidate;

class Repwd
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '重置密码';
    // 参数
    public $_param  = [
        'id'       => '主键',
        'password' => '登录密码',
        'cofimpwd' => '确认登录密码',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('repwd')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $manager = Session::get('manager');

        $num = Db::name('rbac_manager')->where('id',$params['id'])->update(['password'=>md5($params['password'])]);

        return ($num >= 0) ? [201,'重置成功'] : 500;
    }
}