<?php

// 编辑管理员
// PUT admin.rbac.manager.edit

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use app\common\validate\Manager as ManagerValidate;

class Edit
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '编辑管理员';
    // 参数
    public $_param  = [
        'id'       => '主键',
        'role_id'  => '角色',
        'leader'   => '是否组长,0-否 1-是',
        'nickname' => '昵称',
        'status'   => '状态,0-正常 1-锁定',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('edit')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $data['role_id']     = $params['role_id'];
        $data['leader']      = $params['leader'];
        $data['nickname']    = $params['nickname'];
        $data['status']      = $params['status'];
        $num = Db::name('rbac_manager')->where('id',$params['id'])->update($data);

        return ($num >= 0) ? [201,'编辑成功'] : 500;
    }
}