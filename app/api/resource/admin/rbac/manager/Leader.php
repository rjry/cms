<?php

// 设置|取消组长
// PUT admin.rbac.manager.leader

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use app\common\validate\Manager as ManagerValidate;

class Leader
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '设置|取消组长';
    // 参数
    public $_param  = [
        'id'     => '主键',
        'leader' => '是否组长,0-否 1-是',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('leader')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $data['leader'] = $params['leader'];
        $num = Db::name('rbac_manager')->where('id',$params['id'])->update($data);

        return ($num >= 0) ? [201,'修改成功'] : 500;
    }
}