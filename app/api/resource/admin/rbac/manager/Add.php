<?php

// 新增管理员
// POST admin.rbac.manager.add

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use app\common\validate\Manager as ManagerValidate;

class Add
{
    // 方法
    public $_method = ['POST'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '新增管理员';
    // 参数
    public $_param  = [
        'role_id'  => '角色',
        'leader'   => '是否组长,0-否 1-是',
        'username' => '用户名',
        'password' => '登录密码',
        'cofimpwd' => '确认登录密码',
        'nickname' => '昵称',
        'status'   => '状态,0-正常 1-锁定',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('add')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $ret = Db::name('rbac_manager')->where('username',$params['username'])->field('id')->find();
        if ( !is_null($ret) ) return [400,'用户名已存在'];

        $data['role_id']     = $params['role_id'];
        $data['leader']      = $params['leader'];
        $data['username']    = $params['username'];
        $data['password']    = md5($params['password']);
        $data['nickname']    = $params['nickname'];
        $data['status']      = $params['status'];
        $data['create_time'] = time();
        $num = Db::name('rbac_manager')->insert($data);

        return ($num > 0) ? [201,'新增成功'] : 500;
    }
}