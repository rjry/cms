<?php

// 获取我的信息
// GET admin.rbac.manager.uinfo

namespace app\api\resource\admin\rbac\manager;
use think\facade\Session;
use think\Db;

class Uinfo
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = ['manager'];
    // 描述
    public $_description = '获取我的信息';
    // 参数
    public $_param  = [];

    public function run(&$request)
    {
        $manager = Session::get('manager');

        if (0 == $manager['role_id']) {
            $manager['role'] = '超级管理员';
        } else {
            $manager['role'] = Db::name('rbac_role')->where('id',$manager['role_id'])->value('name');
        }

        unset($manager['id'],
              $manager['status'],
              $manager['delete_time']);

        return [200,$manager];
    }
}
