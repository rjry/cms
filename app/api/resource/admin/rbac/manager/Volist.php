<?php

// 获取管理员分页列表
// GET admin.rbac.manager.volist

namespace app\api\resource\admin\rbac\manager;
use app\api\lib\Page;

class Volist
{
    // 方法
    public $_method = ['GET'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '获取管理员分页列表';
    // 参数
    public $_param  = [
        'page'    => '请求页,默认1',
        'show'    => '请求记录数,默认10',
        'role_id' => '角色主键,可选',
        'skey'    => '检索值,可选,用户名|昵称',
    ];

    public function run(&$request)
    {
        $page = $request->param('page/d',1);
        $show = $request->param('show/d',10);

        $rid  = $request->param('role_id/d',0);
        $skey = $request->param('skey/s','','trim');

        $table = 'rbac_manager';
        $where[] = ['role_id','neq',0];
        if (0 != $rid)  $where[] = ['role_id','=',$rid];
        if (0 != $skey) $where[] = ['username|nickname','like','%' . $skey . '%'];
        $order = 'id DESC';
        $pagelist = Page::run($table,$where,$order,$page,$show);

        return [200,$pagelist];
    }
}
