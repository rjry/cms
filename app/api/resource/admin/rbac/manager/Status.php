<?php

// 修改管理员状态
// PUT admin.rbac.manager.status

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use app\common\validate\Manager as ManagerValidate;

class Status
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '修改管理员状态';
    // 参数
    public $_param  = [
        'id'     => '主键',
        'status' => '状态,0-正常 1-锁定',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('status')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $num = Db::name('rbac_manager')->where('id',$params['id'])->update([ 'status'=>$params['status'] ]);

        return ($num >= 0) ? [201,'修改成功'] : 500;
    }
}