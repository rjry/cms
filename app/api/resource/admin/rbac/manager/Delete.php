<?php

// 删除管理员
// DELETE admin.rbac.manager.delete

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use app\common\validate\Manager as ManagerValidate;

class Delete
{
    // 方法
    public $_method = ['DELETE','OPTIONS'];
    // 前置
    public $_pre    = ['superManager'];
    // 描述
    public $_description = '删除管理员';
    // 参数
    public $_param  = [
        'id' => '主键',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('delete')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $num = Db::name('rbac_manager')->where('id',$params['id'])->delete();

        return ($num > 0) ? [204,'删除成功'] : 500;
    }
}