<?php

// 修改密码
// PUT admin.rbac.manager.uppwd

namespace app\api\resource\admin\rbac\manager;
use think\Db;
use think\facade\Session;
use app\common\validate\Manager as ManagerValidate;

class Uppwd
{
    // 方法
    public $_method = ['PUT','OPTIONS'];
    // 前置
    public $_pre    = ['manager'];
    // 描述
    public $_description = '修改密码';
    // 参数
    public $_param  = [
        'passport' => '原登录密码',
        'password' => '登录密码',
        'cofimpwd' => '确认登录密码',
    ];

    public function run(&$request)
    {
        $params = $request->param();
        
        $validate = new ManagerValidate;
        if ( !$validate->scene('uppwd')->check($params) ) {
            $errMsg = $validate->getError();
            return [422,$errMsg];
        }

        $manager = Session::get('manager');

        $id = Db::name('rbac_manager')
        ->where('username',$manager['username'])
        ->where( 'password',md5($params['passport']) )
        ->value('id');

        if ( is_null($id) ) return [401,'原登录密码不正确'];

        $num = Db::name('rbac_manager')->where('id',$id)->update(['password'=>md5($params['password'])]);

        return ($num >= 0) ? [201,'修改成功'] : 500;
    }
}