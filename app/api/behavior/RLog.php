<?php

// [ 记录日志 ]

namespace app\api\behavior;
use think\Db;

class RLog
{
    public function run($data)
    {
        $request = $data[0];
        $result  = $data[1];

        $ip      = $request->ip();
        $method  = $request->method();
        $param   = $request->param();
        if ( !empty($param) ) {
            foreach ($param as $key => $value) {
                $api = $key;
                unset($param[$key]);
                break;
            }
            // 写入数据表
            $data['api']         = $api;
            $data['method']      = $method;
            $data['param']       = json_encode($param,JSON_UNESCAPED_UNICODE);
            $data['result']      = json_encode($result,JSON_UNESCAPED_UNICODE);
            $data['ip']          = $ip;
            $data['create_time'] = time();
            Db::name('api_log')->insert($data);
        }
    }
}
