<?php

// [ 检查前置操作 ]

namespace app\api\behavior;
use app\api\lib\Response;
use think\facade\Session;

class CheckPre
{
    public function run($class)
    {
        $pre = $class->_pre;
        if ( !empty($pre) ) {
            foreach ($pre as $val) $this->{$val}();
        }
    }

    // -----------------------------------------------------------------
    
    // 检查是否管理员
    private function manager()
    {
        Session::has('manager') || Response::send(403,'未授权[管理员]');
    }

    // 检查是否超级管理员
    private function superManager()
    {
        Session::has('manager') || Response::send(403,'未授权[管理员]');
        0 == Session::get('manager.pid') || Response::send(403,'未授权[超级管理员]');
    }
}
