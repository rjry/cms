<?php

// [ 响应类 ]
/*
use app\api\lib\Response;
Response::send($code,$data = null);
*/

namespace app\api\lib;

class Response
{
    // 消息
    private static $_msg = [
        200 => 'Ok',                    // [GET]获取资源成功
        201 => 'Created',               // [POST|PUT]创建只有成功 | 修改资源成功
        202 => 'Accepted',              // [*]请求已经进入后台排队（异步任务）
        204 => 'No Content',            // [DELETE]删除资源成功
        300 => 'Multiple Choices',      // [*]重定向
        400 => 'Bad Request',           // [*]用户发出的请求有错误
        401 => 'Unauthorized',          // [*]未登录 | 令牌、用户名、密码错误
        403 => 'Forbidden',             // [*]未授权
        404 => 'Not Found',             // [*]请求资源不存在
        405 => 'Method Not Allowed',    // [*]请求类型不允许
        422 => 'Unprocessable Entity',  // [*]参数不合法
        423 => 'Locked',                // [*]被锁定
        500 => 'Internal Server Error', // [*]服务器异常
    ];

    // 发送数据到客户端
    public static function send($code, $data = null)
    {
        $msg = isset(self::$_msg[$code]) ? self::$_msg[$code] : 'Undefine';
        header('HTTP/1.1 ' . $code . ' ' . $msg);
        header('Content-Type:application/json;charset=utf-8');
        if ( null === $data || '' === $data ) {
            exit;
        } else {
            if ( $code < 300 ) $result = $data;
            if ( $code >= 300 && $code < 400 ) $result = ['location'=>$data];
            if ( $code >= 400 ) $result = ['error'=>$data];
            echo json_encode($result,JSON_UNESCAPED_UNICODE);
            exit;
        }
    }
}
