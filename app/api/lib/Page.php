<?php

// [ 获取分页数据 ]
/*
use app\api\lib\Page;
$page  = $request->get('page/d',1);
$show  = $request->get('show/d',10);
$table = '';
$where = '';
$order = '';
$pagelist = Page::run($table,$where,$order,$page,$show);
*/

namespace app\api\lib;
use think\Db;

class Page
{
    // 分页数据
    private static $_pagelist = [
        'page' => 1,
        'show' => 10,
        'prev' => 0,
        'next' => 0,
        'list' => [],
    ];

    // 计算分页返回
    public static function run($table, $where, $order, $page, $show)
    {
        self::$_pagelist['page'] = $page;
        self::$_pagelist['show'] = $show;
        self::$_pagelist['prev'] = $page - 1;
        self::$_pagelist['next'] = $page + 1;
        self::$_pagelist['list'] = Db::name($table)->where($where)->order($order)->page($page,$show)->select();
        $ret = Db::name($table)->where($where)->page(self::$_pagelist['next'],$show)->limit(1)->select();
        empty($ret) && self::$_pagelist['next'] = 0;
        return self::$_pagelist;
    }
}
