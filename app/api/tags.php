<?php

// [ 行为绑定配置 ]

return [

    // api请求前监听
    'api_request_before' => [
        # 检查前置操作
        'app\\api\\behavior\\CheckPre',
    ],

    // api请求后监听
    'api_request_after'  => [
        # 记录日志
        // 'app\\api\\behavior\\RLog',
    ],

];
