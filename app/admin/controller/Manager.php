<?php

// [ 键值对 ]

namespace app\admin\controller;
use think\Controller;

class Manager extends Controller
{
    // http://tp.io/admin/manager
    public function index()
    {
        return $this->fetch();
    }
    //添加
    public function add()
    {
        return $this->fetch();
    }
    //编辑
    public function edit()
    {
        return $this->fetch();
    }

}