<?php

// [ 后台管理中心 ]

namespace app\admin\controller;
use think\Controller;

class System extends Controller
{
    // http://tp.io/admin/system
    public function index()
    {
        return $this->fetch();
    }
    
    // http://tp.io/admin/repwd
    public function repwd()
    {
        return $this->fetch();
    }
}