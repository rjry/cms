<?php

// [ 后台 ]

namespace app\admin\controller;
use think\Controller;
use think\facade\Session;

class Index extends Controller
{
    // http://tp.io/admin
    public function index()
    {
        Session::has('manager') ? $this->redirect('@admin/system') : $this->redirect('@admin/login');
    }
}