-- api日志表
DROP TABLE IF EXISTS `api_log`;
CREATE TABLE `api_log` (
`id`           int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
`api`          char(40)      NOT NULL DEFAULT ''     COMMENT '请求接口',
`method`       char(10)      NOT NULL DEFAULT ''     COMMENT '请求方法',
`param`        text                                  COMMENT '请求参数',
`result`       text                                  COMMENT '返回结果',
`ip`           varchar(25)   NOT NULL DEFAULT ''     COMMENT '来源IP',
`create_time`  int(10)       NOT NULL DEFAULT 0      COMMENT '记录时间',
PRIMARY KEY (`id`),
KEY (`api`),
KEY (`ip`),
KEY (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='api日志表';