-- 管理员表
DROP TABLE IF EXISTS `rbac_manager`;
CREATE TABLE `rbac_manager` (
`id`           int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
-- 
`role_id`      int(11)       NOT NULL DEFAULT 0      COMMENT '角色,0-超级管理员',
`leader`       tinyint(1)    NOT NULL DEFAULT 0      COMMENT '是否组长,0-否 1-是',
-- 
`username`     char(20)      NOT NULL DEFAULT ''     COMMENT '用户名',
`password`     char(32)      NOT NULL DEFAULT ''     COMMENT '登录密码',
`nickname`     char(20)      NOT NULL DEFAULT ''     COMMENT '昵称',
-- 
`status`       tinyint(1)    NOT NULL DEFAULT 0      COMMENT '状态,0-正常 1-锁定',
-- 
`create_time`  int(10)       NOT NULL DEFAULT 0      COMMENT '注册时间',
`update_time`  int(10)       NOT NULL DEFAULT 0      COMMENT '活跃时间',
`delete_time`  int(10)       NOT NULL DEFAULT 0      COMMENT '锁定时间',
PRIMARY KEY (`id`),
UNIQUE KEY (`username`),
KEY (`status`),
KEY (`update_time`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8 COMMENT='管理员表';

INSERT INTO `rbac_manager` VALUES (1,0,0,'admin','21232f297a57a5a743894a0e4a801fc3','超级管理员',0,1513143639,0,0);


-- 角色表
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role` (
`id`           int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
`name`         char(20)      NOT NULL DEFAULT ''     COMMENT '名称',
`description`  varchar(80)   NOT NULL DEFAULT ''     COMMENT '描述',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='角色表';


-- 节点表
DROP TABLE IF EXISTS `rbac_node`;
CREATE TABLE `rbac_node` (
`id`           int(11)       NOT NULL AUTO_INCREMENT COMMENT '主键',
`title`        char(20)      NOT NULL DEFAULT ''     COMMENT '名称',
`description`  varchar(80)   NOT NULL DEFAULT ''     COMMENT '描述',
`url`          varchar(255)  NOT NULL DEFAULT ''     COMMENT 'URL',
`pid`          int(11)       NOT NULL DEFAULT 0      COMMENT 'PID',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='节点表';


-- 角色节点表
DROP TABLE IF EXISTS `rbac_role_node`;
CREATE TABLE `rbac_role_node` (
`role_id`      int(11)       NOT NULL DEFAULT 0      COMMENT '角色主键',
`node_id`      int(11)       NOT NULL DEFAULT 0      COMMENT '节点主键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色节点表';

