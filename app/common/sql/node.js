==============================================================
rbac

[管理员]
OK 登录 POST admin.rbac.manager.login
OK 登出 DELETE admin.rbac.manager.logout
OK 修改密码 PUT admin.rbac.manager.uppwd
OK 获取我的信息 GET admin.rbac.manager.uinfo

OK 重置密码 PUT admin.rbac.manager.repwd
OK 设置|取消组长 PUT admin.rbac.manager.leader
OK 修改管理员状态 PUT admin.rbac.manager.status
OK 获取管理员分页列表 GET admin.rbac.manager.volist
OK 获取管理员详情 GET admin.rbac.manager.detail
OK 新增管理员 POST admin.rbac.manager.add
OK 编辑管理员 PUT admin.rbac.manager.edit
OK 删除管理员 DELETE admin.rbac.manager.delete


[角色]
OK 获取角色列表 GET admin.rbac.role.volist
OK 获取角色详情 GET admin.rbac.role.detail
OK 新增角色 POST admin.rbac.role.add
OK 编辑角色 PUT admin.rbac.role.edit
OK 删除角色 DELETE admin.rbac.role.delete
OK 分配角色权限节点 PUT admin.rbac.role.assign


[节点]
OK 获取(角色)权限节点列表 GET admin.rbac.node.volist
OK 获取节点详情 GET admin.rbac.node.detail
OK 新增节点 POST admin.rbac.node.add
OK 编辑节点 PUT admin.rbac.node.edit
OK 删除节点 DELETE admin.rbac.node.delete

==============================================================

[键值对]
OK 获取键值对列表 GET kv.volist
OK 获取键值对详情 GET kv.detail
OK 新增键值对 POST kv.add
OK 编辑键值对 PUT kv.edit
OK 删除键值对 DELETE kv.delete