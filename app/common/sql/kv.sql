-- 键值表
DROP TABLE IF EXISTS `kv`;
CREATE TABLE `kv` (
`key`          char(20)      NOT NULL DEFAULT ''     COMMENT '键名',
`value`        text                                  COMMENT '键值',
PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='键值表';