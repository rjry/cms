<?php

// [ 全局MISS路由 ]

namespace app\common\controller;
use think\Controller;

class Miss extends Controller
{
    public function index()
    {
        return $this->fetch();
    }
}