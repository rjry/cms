<?php

// [ 验证器 ]

namespace app\common\validate;
use think\Validate;

class Tpl extends Validate
{
    // 规则
    protected $rule = [
        'id'     => 'require|integer',
        'mobile' => 'require|mobile',
        'zip'    => 'regex:\d{6}',
    ];

    // 消息
    protected $message = [
        'id.require'     => '主键必须',
        'id.integer'     => '主键必须是数字',
        
        'mobile.require' => '手机号必须',
        'mobile.mobile'  => '手机号格式错误',

        'zip'            => '邮编格式错误',
    ];

    // 场景
    protected $scene = [
        'xxx' => ['xxx','xxx'],
    ];
}
