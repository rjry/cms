<?php

// [ 节点 ]

namespace app\common\validate;
use think\Validate;

class Node extends Validate
{
    // 规则
    protected $rule = [
        'id'          => 'require|integer',
        'title'       => 'require|length:2,20',
        'description' => 'max:80',
        'url'         => 'max:255',
        'pid'         => 'require|integer',
    ];

    // 消息
    protected $message = [
        'id.require'      => '主键必须',
        'id.integer'      => '主键必须是数字',

        'title.require'   => '节点名必须',
        'title.length'    => '节点名长度必须在2~20个字符之间',

        'description.max' => '描述长度不能超过80个字符',

        'url.max'         => 'URL长度不能超过255个字符',

        'pid.require'     => 'PID必须',
        'pid.integer'     => 'PID必须是数字',
    ];

    // 场景
    protected $scene = [
        // 新增节点
        'add'    => ['title','description','url','pid'],
        // 编辑节点
        'edit'   => ['id','title','description','url','pid'],
        // 删除节点
        'delete' => ['id'],
        // 获取节点详情
        'detail' => ['id'],
    ];
}
