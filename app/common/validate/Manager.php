<?php

// [ 管理员 ]

namespace app\common\validate;
use think\Validate;

class Manager extends Validate
{
    // 规则
    protected $rule = [
        'id'       => 'require|integer',
        'username' => 'require|length:4,20',
        'role_id'  => 'require|integer',
        'leader'   => ['regex'=>'/^(0|1)$/'],
        'nickname' => 'require|max:20',
        'password' => 'require|length:4,32',
        'passport' => 'require|length:4,32',
        'password' => 'require|length:4,32',
        'cofimpwd' => 'require|length:4,32|confirm:password',
        'status'   => ['regex'=>'/^(0|1)$/'],
    ];

    // 消息
    protected $message = [
        'id.require'       => '主键必须',
        'id.integer'       => '主键必须是数字',

        'username.require' => '用户名必须',
        'username.length'  => '用户名只能在4~20个字符之间',

        'role_id.require'  => '角色主键必须',
        'role_id.integer'  => '角色主键必须是数字',

        'leader.regex'     => '是否组长只能为0|1',

        'nickname.require' => '昵称必须',
        'nickname.max'     => '昵称长度最多20个字符',

        'passport.require' => '原登录密码必须',
        'passport.length'  => '原登录密码只能在4~32个字符之间',

        'password.require' => '登录密码必须',
        'password.length'  => '登录密码只能在4~32个字符之间',

        'cofimpwd.require' => '确认登录密码必须',
        'cofimpwd.length'  => '确认登录密码只能在4~32个字符之间',
        'cofimpwd.confirm' => '确认登录密码与登录密码不一致',

        'status.regex'     => '状态只能为0|1',
    ];

    // 场景
    protected $scene = [
        // 登录
        'login'  => ['username','password'],
        // 修改密码
        'uppwd'  => ['passport','password','cofimpwd'],
        // 重置密码
        'repwd'  => ['id','password','cofimpwd'],
        // 修改管理员状态
        'status' => ['id','status'],
        // 新增管理员
        'add'    => ['role_id','leader','username','password','cofimpwd','nickname','status'],
        // 编辑管理员
        'edit'   => ['id','role_id','leader','nickname','status'],
        // 删除管理员
        'delete' => ['id'],
        // 获取管理员详情
        'detail' => ['id'],
        // 设置|取消组长
        'leader' => ['id','leader'],
    ];
}
