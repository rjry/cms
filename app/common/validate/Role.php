<?php

// [ 角色 ]

namespace app\common\validate;
use think\Validate;

class Role extends Validate
{
    // 规则
    protected $rule = [
        'id'          => 'require|integer',
        'name'        => 'require|length:2,20',
        'description' => 'max:80',
    ];

    // 消息
    protected $message = [
        'id.require'      => '主键必须',
        'id.integer'      => '主键必须是数字',
        
        'name.require'    => '角色名必须',
        'name.length'     => '角色名长度必须在2~20个字符之间',
        
        'description.max' => '描述长度不能超过80个字符',
    ];

    // 场景
    protected $scene = [
        // 新增角色
        'add'    => ['name','description'],
        // 编辑角色
        'edit'   => ['id','name','description'],
        // 删除角色
        'delete' => ['id'],
        // 获取角色详情
        'detail' => ['id'],
    ];
}