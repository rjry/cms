<?php

// [ 应用入口文件 ]

namespace think;

// 定义分隔符
define('DS', DIRECTORY_SEPARATOR);

// 定义项目根目录
define('ROOT_PATH', __DIR__ . DS);

// 定义应用目录
define('APP_PATH', ROOT_PATH . 'app' . DS);

// 加载框架引导文件
require ROOT_PATH . 'thinkphp' . DS . 'base.php';

// 执行应用并响应
Container::get('app',[APP_PATH])->run()->send();