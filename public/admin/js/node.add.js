$('.sureBtn').on('click',function(){
    var title = $('#title').val();
    var url = $('#url').val();
    var description = $('#description').val();

    $.ajax({
        type: 'post',
        url: '/api?admin.rbac.node.add',
        data: {
            pid: 0,
            title: title,
            url: url,
            description: description
        },
        dataType: 'json',
        success: function(){
            parent.freshNav();
            parent.location.reload();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        },
        complete: function(){
        }
    });
});