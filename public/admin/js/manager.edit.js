var id = tool.GetQueryString("id");
var role_id = tool.GetQueryString("role_id");

//获取角色列表
$.ajax({
    type: 'get',
    url: '/api?admin.rbac.role.volist',
    dataType: 'json',
    success: function(data){
        var e = "";
        for (var i = 0 ; i < data.length ; i++){
            var j = data[i];
            if(j.id == role_id){
                e += "<option value='" + j.id + "' selected = 'selected'>" + j.name +"</option>";
            }else {
                e += "<option value='" + j.id + "'>" + j.name + "</option>";
            }
        }
        $("#role_id").html(e);
    },
    error: function(XmlHttpRequest,textStatus, errorThrown){
    }
});


//获取管理员信息
$.ajax({
    type: 'get',
    url: '/api?admin.rbac.manager.detail',
    data: {
        id: id
    },
    dataType: 'json',
    success: function(data){
        if(data.leader == 1){
            $('#leader').prop('checked',true);
        }
        if(data.status == 1){
            $('#status').prop('checked',true);
        }
        $('#nickname').val(data.nickname);
    },
    error: function(){}
});

//为保存按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var role_id = $('#role_id').val(),
        leader = $('#leader').prop('checked') == true?1:0,
        nickname = $('#nickname').val(),
        status = $('#status').prop('checked') == true?1:0;

    $.ajax({
        type: 'put',
        url: '/api?admin.rbac.manager.edit',
        data: {
            id: id,
            role_id: role_id,
            leader: leader,
            nickname: nickname,
            status: status
        },
        dataType: 'json',
        success: function(){
            parent.location.reload();
            //window.location.href = "/admin/manager";
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        },
        complete: function(){
        }
    });
});