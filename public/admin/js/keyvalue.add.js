//为添加按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var key = $('#title').val();
    var value = $('#content').val();

    $.ajax({
        type: 'post',
        url: '/api?kv.add',
        data: {
            key: key,
            value: value
        },
        dataType: 'json',
        success: function(){
            parent.location.reload();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        },
        complete: function(){
        }
    });
});
