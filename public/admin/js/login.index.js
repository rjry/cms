//为登录按钮绑定点击事件
$('#loginBtn').on("click",function(){
    //获取用户输入的账号密码
    var username = $('#userName').val();
    var password = $('#userPwd').val();

    $.ajax({
        type:"post",
        url: '/api?admin.rbac.manager.login',
        data:{
            username: username,
            password: password
        },
        dataType: "json",
        success: function(){
            window.location.href = "/admin/system.html";
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        }
    });
});