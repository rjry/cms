//获取角色列表
$.ajax({
    type: 'get',
    url: '/api?admin.rbac.role.volist',
    dataType: 'json',
    success: function(data){
        var e = "";
        for (var i = 0 ; i < data.length ; i++){
            var j = data[i];
            e += "<option value='" + j.id + "'>" + j.name +"</option>";
        }
        $("#role_id").html(e);
    },
    error: function(XmlHttpRequest,textStatus, errorThrown){
    }
});



//为添加按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var role_id = $('#role_id').val(),
        leader = $('#leader').prop('checked') == true?1:0,
        username = $('#username').val(),
        password = $('#password').val(),
        cofimpwd = $('#cofimpwd').val(),
        nickname = $('#nickname').val(),
        status = $('#status').prop('checked') == true?1:0;
    $.ajax({
        type: 'post',
        url: '/api?admin.rbac.manager.add',
        data: {
            role_id: role_id,
            leader: leader,
            username: username,
            password: password,
            cofimpwd: cofimpwd,
            nickname: nickname,
            status: status
        },
        dataType: 'json',
        success: function(){
            parent.location.reload();
            //$('form')[0].reset();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        }
    });
});
