window.onload = function() {
    //获取用户信息
    $.ajax({
        type: "get",
        url: '/api?admin.rbac.manager.uinfo',
        success: function (data) {
            getNav(data.role_id);
            if (data.role_id == 0) { //若为超级管理员则为页面添加超级管理员按钮
                var e = '';
                e += "<div class='pull-right adminChange btn-group dropdown'>";
                e += "<button type='button' id='dropBtn' class='dropdown-toggle' data-toggle='dropdown'>";
                e += "权限管理<span class='caret'></span>";
                e += "</button>";
                e += "<ul class='dropdown-menu' role='menu' aria-labelledby='dropBtn'>";
                e += "<li><a target='pageFrame' href='/admin/manager'>管理员管理</a></li>";
                e += "<li><a target='pageFrame' href='/admin/role'>角色管理</a></li>";
                e += "<li><a target='pageFrame' href='/admin/node'>节点管理</a></li>";
                e += "</ul>";
                e += "</div>";
                e += "<button class='pull-right keyvalue'><a target='pageFrame' href='/admin/keyvalue'>键值对</a></button>";
                $('.adminBtn').html(e);
            }
        }
    });
}
//获取当前role_id角色所拥有权限的导航项
function getNav(role_id){
    $.ajax({
        type: 'get',
        url: '/api?admin.rbac.node.volist',
        data: {
            role_id: role_id
        },
        success: function (data) {
            initSideNav(data);
        }
    });
}
//为页面动态添加导航栏
function initSideNav(menu) {
    function createmenu(menu) {
        var ele = '';
        for (var i in menu) {
            var list = menu[i];
            var isChild = list.child instanceof Array && list.child.length > 0 ? true : false;
            ele += '<li class="">';
            if (isChild) {
                ele += '<a href="javascript:;" class="dropdown-toggle">';
                ele += "<i class='fa fa-list'></i>";
            } else {
                ele += '<a target="pageFrame" href="' + (list.url == '' ? 'javascript:;' : list.url) + '">';
                ele += "<i class='fa fa-tags'></i>";
            }
            ele += '<span class="menu-text"> ' + list.title + ' </span>';
            if (isChild) {
                ele += '<b class="arrow fa fa-angle-down"></b>';
            }
            ele += '</a>';
            if (isChild) {
                ele += '<ul class="submenu">';
                ele += createmenu(list.child);
                ele += '</ul>';
            }
            ele += '</li>';
        }
        return ele;
    }

    var $nav = $('.nav-list');
    $nav.html(createmenu(menu));
    // 存储上一次点击下标
    var prevli = null;
    $nav.find('li').each(function (index) {
        var that = $(this);
        that.off('click').on('click', function (e) {
            var e = e || window.event;
            // 阻止冒泡
            e.stopPropagation();
            // 判断是否点击过
            if (prevli != null) {
                // 清除上次点击样式
                $nav.find('li:eq(' + prevli + ')').removeClass('active').parents('li').removeClass('active');
            }
            // 设置本次点击下标
            prevli = index;
            // 获取下级菜单
            var $submenu = that.children('.submenu');
            // 判断是否有下级菜单
            if ($submenu.length > 0) {
                // 有下级菜单，展开下级菜单
                that.addClass('open');
                $submenu.slideToggle(200);
            } else {
                // 没有下级
                that.addClass('active').parents('li.open').addClass('active');
            }
        });
    });
}


//为退出按钮绑定点击事件
$('#logout').on("click", function () {
    $.ajax({
        type: "delete",
        url: '/api?admin.rbac.manager.logout',
        complete: function () {
            window.location.href = "/admin";
        }
    });
});