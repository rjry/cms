//获取管理员列表
var nowPage = 1;
managerVolist(nowPage);
function managerVolist(nowPage){
    $.ajax({
        type: 'get',
        url: '/api?admin.rbac.manager.volist',
        data: {
            page: nowPage,
            show: '10',
            role_id: '0'
        },
        dataType: 'json',
        success: function(data){
            $('.nowPage').text(nowPage);
            createManager(data);
        },
        error: function(){}
    });
}


function createManager(data){
    $.ajax({
        type: 'get',
        url: '/api?admin.rbac.role.volist',
        success: function(roleData){
            var e = '';
            for (var i = 0 ; i < data.list.length ; i++){
                var j = data.list[i];
                e += "<tr>";
                for(x = 0 ; x < roleData.length ; x++){
                    if(roleData[x].id == j.role_id){
                        e += "<td>" + roleData[x].name +"</td>";
                        break;
                    }
                }
                e += "<td>" + j.username +"</td>";
                e += "<td>" + j.nickname +"</td>";
                e += "<td>";
                if(j.leader == 0){
                    e += "<input type='checkbox'  data-id='" + j.id + "' data-leader='" + j.leader + "' class='leader mui-switch'/>";
                    //e += "否";
                }else if(j.leader == 1){
                    e += "<input type='checkbox' checked  data-id='" + j.id + "' data-leader='" + j.leader + "' class='leader mui-switch'/>"
                    //e += "是";
                }
                e += "</td>";
                e += "<td>" + tool.formatDate('yy-mm-dd hh:ii:ss', j.create_time) +"</td>";
                e += "<td>" + tool.formatDate('yy-mm-dd hh:ii:ss', j.update_time) +"</td>";
                e += "<td>";
                if(j.status == 0){
                    e += "<input type='checkbox'  data-id='" + j.id + "' data-status='" + j.status + "' class='status mui-switch'/>";
                    //e += "正常";
                }else if(j.status == 1){
                    e += "<input type='checkbox' checked  data-id='" + j.id + "' data-status='" + j.status + "' class='status mui-switch'/>";
                    //e += "已锁定";
                }
                e += "</td>";
                e += "<td>";
                e += "<div class='btn-group'>";
                e += "<button title='编辑' data-roleId='"+ j.role_id +"' data-id='"+ j.id +"' class='editBtn btn btn-info btn-sm'><span class='glyphicon glyphicon-pencil'></span></button>";
                e += "<button title='删除' data-id='"+ j.id +"' class='deleteBtn btn btn-danger btn-sm' data-toggle='modal' data-target='#delModal'><span class='glyphicon glyphicon-trash'></span></button>";
                e += "</div>";
                e += "</td>";
                e += "</tr>";
            }
            $('tbody').html(e);

            var f = '';
            f += " <ul class='pagination'>";
            f += "<li><a data-prev='" + data.prev + "' class='prev' href='javascript:;'>&laquo;</a></li>";
            if(data.prev !== 0){
                f += "<li><a data-prev='" + data.prev + "' class='prev' href='javascript:;'>" + data.prev + "</a></li>";
            }
            f += "<li><a class='nowPage' href='javascript:;'>" + data.page + "</a></li>";
            if(data.next !== 0){
                f += "<li><a data-next='" + data.next + "' class='next' href='javascript:;'>" + data.next + "</a></li>";
            }
            f += "<li><a data-next='" + data.next + "' class='next' href='javascript:;'>&raquo;</a></li>";

            $('.pageControl').html(f);
        },
        error: function(){}
    });
}

//为上一页绑定点击事件
$('.pageControl').on('click','.prev',function(){
    if($(this).attr('data-prev') > 0){
        nowPage = $(this).attr('data-prev');
        managerVolist(nowPage);
    }
});

//为下一页绑定点击事件
$('.pageControl').on('click','.next',function(){
    if($(this).attr('data-next') > 0){
        nowPage = $(this).attr('data-next');
        managerVolist(nowPage);
    }
});

//为是否组长绑定点击事件
$('tbody').on('click','.leader',function(){
    var id = $(this).attr('data-id');
    var leader = $(this).attr('data-leader') == 0?1:0;
    $.ajax({
        type: 'put',
        url: '/api?admin.rbac.manager.leader',
        data: {
            id: id,
            leader: leader
        },
        success: function(){},
        error: function(){}
    });
});

//为状态绑定点击事件
$('tbody').on('click','.status',function(){
    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status') == 0?1:0;
    $.ajax({
        type: 'put',
        url: '/api?admin.rbac.manager.status',
        data: {
            id: id,
            status: status
        },
        success: function(){},
        error: function(){}
    });
});


//为删除按钮绑定点击事件
var id = '';
$('tbody').on('click','.deleteBtn',function(){
    id = $(this).attr('data-id');
});

$('.sureDel').on('click',function(){
    $.ajax({
        type: 'delete',
        url: '/api?admin.rbac.manager.delete',
        data: {
            id: id
        },
        dataType: 'json',
        success: function(){
        },
        error: function(){},
        complete: function(){
            managerVolist(nowPage);
            id = '';
        }
    });
});

//为编辑按钮绑定点击事件
$('tbody').on('click','.editBtn',function(){
    id = $(this).attr('data-id');
    var role_id = $(this).attr('data-roleId');
    $('#addModal').modal();
    $('#addFrame').attr('src',"/admin/manager/edit?id=" + id +"&role_id=" + role_id);
});


////为添加按钮绑定点击跳转事件
$('.addBtn').on('click',function(event){
    event.preventDefault()
    var _this = $(this);
    $('#addModal').modal();
    $('#addFrame').attr('src',_this.attr('href'));
});

