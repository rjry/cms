//获取节点列表
nodeVolist();
function freshNav(){
    parent.getNav(0);
}
function nodeVolist() {
    $.ajax({
        type: 'get',
        url: '/api?admin.rbac.node.volist',
        data: {
            role_id: 0
        },
        success: function (data) {
            $('.nodeUl').html(createNode(data));
        },
        error: function () {
        }
    });
}
//循环输出节点列表
function createNode(data){
    var e = '';
    for(var i in data){
        var list = data[i];
        var isChild = list.child instanceof Array && list.child.length > 0 ? true : false;
        e += "<li>";
        e += "<span><span class='glyphicon glyphicon-tags'></span>" + list.title +"</span>";
        e += "<div class='btn-group'>";
        e += "<button title='添加子节点' data-pid='" + list.pid + "'  data-id='" + list.id +"' class='addChild btn btn-sm btn-success'><span class='glyphicon glyphicon-plus'></span></button>";
        e += "<button title='编辑' data-pid='" + list.pid + "'  data-id='" + list.id + "' class='nodeEdit btn btn-sm btn-info'><span class='glyphicon glyphicon-pencil'></span></button>";
        e += "<button title='删除' data-pid='" + list.pid + "'  data-id='" + list.id +"' class='nodeDel btn btn-sm btn-danger' data-toggle='modal' data-target='#delModal'><span class='glyphicon glyphicon-trash'></span></button>";
        e += "</div>";
        if(isChild){
            e += "<ul>";
            e += createNode(list.child);
            e += "</ul>";
        }
        e += "</li>";
    }
    return e;
}
//为添加一级节点按钮绑定点击跳转事件
$('.addBtn').on('click',function(){
    event.preventDefault()
    var _this = $(this);
    $('#addModal').modal();
    $('#addFrame').attr('src',_this.attr('href'));
});
//为添加子节点按钮绑定点击跳转事件
$('.nodeUl').on('click','.addChild',function(){
    var pid = $(this).attr('data-id');
    $('#addModal').modal();
    $('#addFrame').attr('src','/admin/node/addchild?pid='+ pid);
});
//为编辑按钮绑定点击跳转事件
$('.nodeUl').on('click','.nodeEdit',function(){
    var id = $(this).attr('data-id');
    var pid = $(this).attr('data-pid');
    $('#addModal').modal();
    $('#addFrame').attr('src', '/admin/node/edit?id='+ id + '&pid='+ pid);
});
var id2 = '';
//为删除按钮绑定点击事件
$('.nodeUl').on('click','.nodeDel',function(){
    id2 = $(this).attr('data-id');
});
//模态框删除按钮
$('.sureDel').on('click',function(){
    $.ajax({
        type: 'delete',
        url: '/api?admin.rbac.node.delete',
        data: {
            id: id2
        },
        dataType: 'json',
        success: function(){
            id2 = '';
            nodeVolist();
            freshNav();
        },
        error: function(){},
        complete: function(){
        }
    });
});

