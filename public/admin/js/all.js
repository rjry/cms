//富文本框
var E = window.wangEditor;
var editor;

// 初始化编辑器上传参数
function initVarEditor(we, accp) {
    we.customConfig.uploadImgServer = data_qiniu.action;
    we.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
    we.customConfig.uploadImgMaxLength = 1;
    we.customConfig.uploadImgParams = {
        key: initImageKey(accp),
        token: initUploadToken()
    };
    we.customConfig.uploadFileName = 'file';
    we.customConfig.customAlert = function(info) {
        console.dir('自定义提示：' + info);
    }
    we.customConfig.uploadImgHooks = {
        customInsert: function(insertImg, result, editor) {
            var url = data_qiniu.domain + result.key;
            insertImg(url);
        }
    }
}