var role_id = tool.GetQueryString("id");
var idArray = [];

//获取该角色用的权限的节点数组
$.ajax({
    type: 'get',
    url: '/api?admin.rbac.node.volist',
    data: {
        role_id: role_id
    },
    dataType: 'json',
    success: function(data) {
        getArray(data);
        roleNodeVolist();
    },
    error: function(){},
    complete: function(){
    }
});

function getArray(data){
    for (v in data) {
        var k = data[v]
        idArray.push(k.id);
        if (k.child) {
            getArray(k.child);
        }
    }
}

//获取所有节点
function roleNodeVolist() {
    $.ajax({
        type: 'get',
        url: '/api?admin.rbac.node.volist',
        data: {},
        dataType: 'json',
        success: function (data) {
            $('.nodeUl').html(createNode(data));
        },
        error: function () {
        },
        complete: function () {
        }
    });
}

function createNode(data){
    var e = '';
    for(var i in data){
        var list = data[i];
        var isChild = list.child instanceof Array && list.child.length > 0 ? true : false;
        e += "<li>";
        e += "<span><span class='glyphicon glyphicon-tags'></span>" + list.title +"</span>";
        e += "<div class='btn-group'>";
        if($.inArray(Number(list.id),idArray) == -1?false:true){
            e += "<input data-id='" + list.id + "' class='checkbox mui-switch' checked type='checkbox'/>";
        }else{
            e += "<input data-id='" + list.id + "' class='checkbox mui-switch' type='checkbox'/>";
        }
        e += "</div>";
        if(isChild){
            e += "<ul>";
            e += createNode(list.child);
            e += "</ul>";
        }
        e += "</li>";
    }
    return e;
}

$('.nodeUl').on('click','.checkbox',function(){
    var allArray = [];
    if($(this).prop('checked') == false){
        $(this).parent().parent().find('ul').find('li').find('div').find('.checkbox').prop('checked',false);
        //$(this).parent().find('checkbox').prop('checked',false);
    }
    var checkBtn = $('.checkbox');
    for(var i = 0 ; i < checkBtn.length ; i++){
        var thisCheckBtn = checkBtn.eq(i);
        if(thisCheckBtn.prop('checked') == true){
            allArray.push(Number(thisCheckBtn.attr('data-id')));
        }
    }
    $.ajax({
        type: 'put',
        url: '/api?admin.rbac.role.assign',
        data: {
            role_id: role_id,
            item: allArray
        },
        success: function(){
        },
        error: function(){}
    });
});
//function createNode(data){
//    var e = '';
//    for (v in data){
//        var k = data[v];
//        e += "<div class='nodeCell panel panel-default'>";
//        e += "<div class='firstNode'>";
//        e += "<span class='nodeTitle'><span class='glyphicon glyphicon-tags'></span>" + k.title +"</span>";
//        if($.inArray(Number(k.id),idArray) == -1?false:true){
//            e += "<input data-id='" + k.id + "' class='checkbox firstCheck mui-switch' checked type='checkbox'/>";
//        }else{
//            e += "<input data-id='" + k.id + "' class='checkbox firstCheck mui-switch' type='checkbox'/>";
//        }
//        e += "</div>";
//        if(k.child){
//            for (v2 in k.child){
//                var l =  k.child[v2];
//                e += "<div class='childNode panel panel-default'>";
//                e += "<span class='nodeTitle'><span class='line'>------</span> <span class='glyphicon glyphicon-tags'></span>" + l.title + "</span>";
//                if($.inArray(Number(l.id),idArray) == -1?false:true){
//                    e += "<input data-id='" + l.id + "' checked class='checkbox childCheck mui-switch' type='checkbox'/>";
//                }else{
//                    e += "<input data-id='" + l.id + "' class='checkbox childCheck mui-switch' type='checkbox'/>";
//                }
//                e += "</div>";
//            }
//        }
//        e += "</div>";
//        $('.nodeBox').html(e);
//
//        $(".childCheck").on("click",function(){
//            if($(this).parent().prevAll('.firstNode').find('.firstCheck').prop('checked') == true){
//                var id = $(this).attr("data-id");
//                if($.inArray(Number(id),idArray) == -1?true:false){
//                idArray.push(Number(id));
//                } else{
//                    removeArray(idArray,Number(id));
//                }
//                $.ajax({
//                    type: 'put',
//                    url: '/api?admin.rbac.role.assign',
//                    data: {
//                        role_id: role_id,
//                        item: idArray
//                    },
//                    success: function(){
//                    },
//                    error: function(){}
//                });
//            }else{
//                $(this).prop('checked',false);
//            }
//        });
//        $(".firstCheck").on("click",function(){
//            var id = $(this).attr("data-id");
//            if($(this).prop('checked') == false){
//                $(this).parent().nextAll('.childNode').find('.childCheck').prop('checked',false);
//                if($.inArray(Number(id),idArray) == -1?true:false){
//                    idArray.push(Number(id));
//                } else{
//                    removeArray(idArray,Number(id));
//                }
//                $.ajax({
//                    type: 'put',
//                    url: '/api?admin.rbac.role.assign',
//                    data: {
//                        role_id: role_id,
//                        item: idArray
//                    },
//                    success: function(){
//                    },
//                    error: function(){}
//                });
//            }else{
//                if($.inArray(Number(id),idArray) == -1?true:false){
//                    idArray.push(Number(id));
//                } else{
//                    removeArray(idArray,Number(id));
//                }
//                $.ajax({
//                    type: 'put',
//                    url: '/api?admin.rbac.role.assign',
//                    data: {
//                        role_id: role_id,
//                        item: idArray
//                    },
//                    success: function(){
//                        window.location.reload();
//                    },
//                    error: function(){}
//                });
//            }
//
//        });
//    }
//}

function removeArray(arr, val) {
    for(var i=0; i<arr.length; i++) {
        if(arr[i] == val) {
            arr.splice(i, 1);
            break;
        }
    }
}


