//获取角色列表
roleVolist();
function roleVolist(){
    $.ajax({
        type: 'get',
        url: '/api?admin.rbac.role.volist',
        success: function(data){
            var e = '';
            for (var i = 0 ; i < data.length ; i++){
                var j = data[i];
                e += "<tr>";
                e += "<td>" + j.name + "</td>";
                e += "<td>" + j.description + "</td>";
                e += "<td>";
                e += "<div class='btn-group'>";
                e += "<button title='分配权限' data-id='"+ j.id +"' class='autBtn btn btn-success btn-sm'><span class='glyphicon glyphicon-flag'></span></button>";
                e += "<button title='编辑' data-id='"+ j.id +"' class='editBtn btn btn-info btn-sm'><span class='glyphicon glyphicon-pencil'></span></button>";
                e += "<button title='删除' data-id='"+ j.id +"' class='deleteBtn btn btn-danger btn-sm' data-toggle='modal' data-target='#delModal'><span class='glyphicon glyphicon-trash'></span></button>";
                e += "</div>";
                e += "</td>";
                e += "</tr>";
            }
            $('tbody').html(e);
        }
    });
}
var id = '';
//为删除按钮以及模态框确认按钮绑定点击事件
$('tbody').on('click','.deleteBtn',function(){
    id = $(this).attr("data-id");
});

$('.sureDel').on('click',function(){
    $.ajax({
        type: 'delete',
        url: '/api?admin.rbac.role.delete',
        data: {
            id: id
        },
        dataType: 'json',
        success: function(){
        },
        error: function(){},
        complete: function(){
            roleVolist();
            id = '';
        }
    });
});

//为分配权限按钮绑定点击跳转事件
$('tbody').on('click','.autBtn',function(){
    id = $(this).attr("data-id");
    $('#addModal').modal();
    $('#addFrame').attr('src','/admin/role/aut?id='+id);
});


//为编辑按钮绑定点击事件
$('tbody').on('click','.editBtn',function(event){
    event.preventDefault()
    id = $(this).attr("data-id");
    var _this = $(this);
    $('#addModal').modal();
    $('#addFrame').attr('src','/admin/role/edit?id='+id);
});


//为添加按钮绑定点击事件
$('.addBtn').on('click',function(event){
    event.preventDefault()
    var _this = $(this);
    $('#addModal').modal();
    $('#addFrame').attr('src',_this.attr('href'));
});