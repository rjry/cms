//为添加按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var name = $('#name').val(),
        description = $('#description').val();
    $.ajax({
        type: 'post',
        url: '/api?admin.rbac.role.add',
        data: {
            name: name,
            description: description
        },
        dataType: 'json',
        success: function(){
            parent.location.reload();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        }
    });
});
//为取消按钮绑定点击事件
$('.quitBtn').on('click',function(){
    window.location.href = '/admin/role';
});