var id = tool.GetQueryString("id");
var pid = tool.GetQueryString("pid");
//获取该项名称描述url
$.ajax({
    type: 'get',
    url: '/api?admin.rbac.node.detail',
    data: {
        id: id
    },
    success: function(data){
        $('#title').val(data.title);
        $('#url').val(data.url);
        $('#description').val(data.description);
    },
    error: function(){}
});
//为保存按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var title = $('#title').val();
    var description = $('#description').val();
    var url = $('#url').val();
    $.ajax({
        type: 'put',
        url: '/api?admin.rbac.node.edit',
        data: {
            id: id,
            pid: pid,
            title: title,
            description: description,
            url: url
        },
        dataType: 'json',
        success: function(){
            parent.freshNav();
            parent.location.reload();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        },
        complete: function(){
        }
    });
});