//为更改按钮绑定点击事件
$('.pwdChange').on('click',function(){
    //获取原密码与新密码
    var passport = $('#passport').val();
    var password = $('#password').val();
    var cofimpwd = $('#cofimpwd').val();

    $.ajax({
        type:"put",
        url: '/api?admin.rbac.manager.uppwd',
        data: {
            passport: passport,
            password: password,
            cofimpwd: cofimpwd
        },
        dataType: "json",
        success: function(){
            alert("密码修改成功");
            $('#passport').val('');
            $('#password').val('');
            $('#cofimpwd').val('');
            parent.location.reload();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];//分解出错误信息字段
            $('.modal-title').html("错误信息：" + errorInfo);//输出错误信息
            $('#errorModal').modal();//显示错误信息模态框
        }
    });
});

//为取消按钮绑定点击事件
$('.pwdChangeQuit').on('click',function(){
    parent.location.reload();
});