//获取键值对列表
kvVolist();

function kvVolist(){
$.ajax({
   type: 'get',
   url: '/api?kv.volist',
   success: function(data){
        var e = '';
        for (var i = 0 ; i < data.length ; i++){
            var j = data[i];
            e += "<li>";
            e += "<span class='key'>" + j.key + "</span>";
            e += "<span class='value'>" + j.value + "</span>";
            e += "<div class='btn-group'>";
            e += "<button title='编辑' data-id='"+ j.key +"' class='editBtn btn btn-sm btn-info'><span class='glyphicon glyphicon-pencil'></span></button>";
            e += "<button title='编辑' data-id='"+ j.key +"' class='deleteBtn btn btn-sm btn-danger' data-toggle='modal' data-target='#delModal'><span class='glyphicon glyphicon-trash'></span></button>";
            e += "</div>";
            e += "</li>";
        }
        $('.kvUl').html(e);
   },
   error: function(){}
});
}

var key = '';
//为删除按钮以及模态框确认按钮绑定点击事件
$('.kvUl').on('click','.deleteBtn',function(){
    key = $(this).attr("data-id");
});

$('.sureDel').on('click',function(){
    $.ajax({
        type: 'delete',
        url: '/api?kv.delete',
        data: {
            key: key
        },
        dataType: 'json',
        success: function(){
        },
        error: function(){},
        complete: function(){
            kvVolist();
            key = '';
        }
    });
});



//为编辑按钮绑定点击事件
$('.kvUl').on('click','.editBtn',function(){
    key = $(this).attr("data-id");
    $('#addModal').modal();
    $('#addFrame').attr('src','/admin/keyvalue/edit?key='+key);
});




//为添加按钮绑定点击跳转事件
$('.addBtn').on('click',function(event){
    event.preventDefault()
    var _this = $(this);
    $('#addModal').modal();
    $('#addFrame').attr('src',_this.attr('href'));
});