var urlkey = tool.GetQueryString("key");

//获取所选项的键值
$.ajax({
    type: 'get',
    url: '/api?kv.detail',
    data: {
        key: urlkey
    },
    dataType: 'json',
    success: function(data){
        $('#title').val(data.key);
        $('#content').val(data.value);
    },
    error: function(){},
    complete: function(){
    }
});

//为保存按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var key = $('#title').val();
    var value = $('#content').val();

    $.ajax({
        type: 'put',
        url: '/api?kv.edit',
        data: {
            key: key,
            value: value
        },
        dataType: 'json',
        success: function(){
            parent.location.reload();
        },
        error: function(){},
        complete: function(){
        }
    });
});
