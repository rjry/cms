var urlid = tool.GetQueryString("id");

//获取该id的角色名和描述
$.ajax({
    type: 'get',
    url: '/api?admin.rbac.role.detail',
    data: {
        id: urlid
    },
    dataType: 'json',
    success: function(data){
        $('#name').val(data.name);
        $('#description').val(data.description);
    },
    error: function(){}
});

//为保存按钮绑定点击事件
$('.sureBtn').on('click',function(){
    var id = urlid;
    var name = $('#name').val();
    var description = $('#description').val();

    $.ajax({
        type: 'put',
        url: '/api?admin.rbac.role.edit',
        data: {
            id: id,
            name: name,
            description: description
        },
        dataType: 'json',
        success: function(){
            parent.location.reload();
        },
        error: function(XmlHttpRequest,textStatus, errorThrown){
            var errorInfo = XmlHttpRequest.responseText.split(":")[1].split("}")[0].split("\"")[1];
            $('.modal-title').html("错误信息：" + errorInfo);
            $('#errorModal').modal();
        },
        complete: function(){
        }
    });
});

//为取消按钮绑定点击事件
$('.quitBtn').on('click',function(){
    window.location.href = "/admin/role";
});