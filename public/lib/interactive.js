! function(win) {

    var ajax = function(opts) {

        var xhr = function() {
            var _xhr = null;
            try {
                _xhr = new XMLHttpRequest();
            } catch(e) {
                var IEXHRVers = ["Msxml3.XMLHTTP", "Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];
                for(var i = 0, len = IEXHRVers.length; i < len; i++) {
                    try {
                        _xhr = new ActiveXObject(IEXHRVers[i]);
                    } catch(e) {
                        continue;
                    }
                }
            }
            return _xhr;
        }();

        var init = function(opts) {
            var opt = tool.optionExtend({
                type: 'get',
                url: '',
                data: {},
                async: true,
                timeout: 5000,
                header: {},
                beforeSend: function() {},
                complete: function(XMLHttpRequest, textStatus) {}
            }, opts);

            // 设置超时
            var data_timeout = {};

            var data_opt = '';
            if(opt.data instanceof Object) {
                var data_arrs = [];
                for(var i in opt.data) {
                    data_arrs.push(i + '=' + opt.data[i]);
                }
                data_opt = data_arrs.join('&');
            } else if(typeof opt.data == 'string') {
                data_opt = opt.data;
            }

            var readystatechange = function() {
                switch(xhr.readyState) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        var result = {
                            'status': xhr.status
                        };
                        if((xhr.responseText !== '' || xhr.status == 204) && xhr.status != 500) {
                            var response = '';
                            (xhr.responseText !== '') && (response = JSON.parse(xhr.responseText));
                            if(xhr.status == 200 || xhr.status == 201 || xhr.status == 204 || xhr.status == 302) {
                                result['state'] = true;
                                result['data'] = response;
                            } else {
                                result['state'] = false;
                                result['error'] = response.error;
                            }
                        } else {
                            result['data'] = xhr.responseText;
                            if(typeof judexport == 'boolean' && judexport === false) {

                            } else {
                                resetServiceConnect(result);
                                return;
                            }
                        }
                        clearTimeout(data_timeout[strtime]);
                        delete data_timeout[strtime];
                        opt.complete(result);
                        break;
                    default:
                        break;
                }
            };
            var data_url = opt.url;
            if(opt.type === 'get' && data_opt !== '') {
                if((/\?/gi).test(opt.url)) {
                    data_url += '&' + data_opt;
                } else {
                    data_url += '?' + data_opt;
                }
            }
            xhr.open(opt.type, data_url, opt.async);
            if(opt.async === true) {
                xhr.onreadystatechange = readystatechange;
            }
            if(opt.type == 'post' || opt.type == 'put' || opt.type == 'delete') {
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            }
            for(var i in opt.header) {
                xhr.setRequestHeader(i, opt.header[i]);
            }
            var strtime = new Date().getTime();
            data_timeout[strtime] = setTimeout(function() {
                xhr.abort();
                opt.complete({
                    status: 504,
                    state: false,
                    error: '请求超时'
                });
            }, opt.timeout);
            xhr.send(opt.type === 'get' ? null : data_opt);
            if(opt.async === true) {
                readystatechange();
            }
        }

        this.get = function() {
            var url = arguments[0] || '';
            if(url == '') return;
            var data = (arguments[1] && typeof arguments[1] == 'object') ? arguments[1] : {};
            var fn = (arguments[1] && typeof arguments[1] == 'function') ? arguments[1] : (arguments[2] || function() {});
            init({
                'type': 'get',
                'url': url,
                'data': data,
                'complete': fn
            });
        }

        this.post = function() {
            var url = arguments[0] || '';
            if(url == '') return;
            var data = (arguments[1] && typeof arguments[1] == 'object') ? arguments[1] : {};
            var fn = (arguments[1] && typeof arguments[1] == 'function') ? arguments[1] : (arguments[2] || function() {});
            init({
                'type': 'post',
                'url': url,
                'data': data,
                'complete': fn
            });
        }

        this.put = function() {
            var url = arguments[0] || '';
            if(url == '') return;
            var data = (arguments[1] && typeof arguments[1] == 'object') ? arguments[1] : {};
            var fn = (arguments[1] && typeof arguments[1] == 'function') ? arguments[1] : (arguments[2] || function() {});
            init({
                'type': 'put',
                'url': url,
                'data': data,
                'complete': fn
            });
        }

        this.del = function() {
            var url = arguments[0] || '';
            if(url == '') return;
            var data = (arguments[1] && typeof arguments[1] == 'object') ? arguments[1] : {};
            var fn = (arguments[1] && typeof arguments[1] == 'function') ? arguments[1] : (arguments[2] || function() {});
            init({
                'type': 'delete',
                'url': url,
                'data': data,
                'complete': fn
            });
        }

        if(typeof opts == 'object') {
            init(opts);
        } else {
            return this;
        }
    }

    win.ajax = ajax;
}(window);